﻿package fahrkartenautomat;

import java.util.Scanner;

class Fahrkartenautomat {

	private static Scanner tastatur;

	/**
	 * Erfasst die Bestellung
	 * 
	 * @return Gesamter Ticketpreis
	 */
	public static double fahrkartenbestellungErfassen() {
		
		String[] bezeichungliste = { "Einzelfahrschein Regeltarif AB" , 
									 "Einzelfahrschein Berlin BC" ,
									 "Einzelfahrschein Berlin ABC" ,
									 "Kurzstrecke" ,
									 "Tageskarte Regeltarif AB",
									 "Tageskarte Berlin BC" ,
									 "Tageskarte Berlin ABC" ,
									 "Kleingruppen-Tageskarte Regeltarif AB" ,
									 "Kleingruppen-Tageskarte Berlin BC" ,
									 "Kleingruppen-Tageskarte Berlin ABC"};
		int anzahlTickets;
		double ticketPreis = 0.0;
		tastatur = new Scanner(System.in);

		int auswahl;

		System.out.println("Fahrkartenbestellvorgang:");
		System.out.println("===========================\n");
		System.out.println("Waehlen Sie ihre Wunschfahrkarte für Berlin aus:");

		System.out.println("  " + bezeichungliste[0] + "[2,90 EUR] (1)");
		System.out.println("  " + bezeichungliste[1] + " [3,30 EUR] (2)");
		System.out.println("  " + bezeichungliste[2] + " [3,60 EUR] (3)");
		System.out.println("  " + bezeichungliste[3] + " [1,90 EUR] (4)");
		System.out.println("  " + bezeichungliste[4] + " [8,60 EUR] (5)");
		System.out.println("  " + bezeichungliste[5] + " [9,00 EUR] (6)");
		System.out.println("  " + bezeichungliste[6] + " [23,50 EUR] (7)");
		System.out.println("  " + bezeichungliste[7] + " [24,30 EUR] (8)");
		System.out.println("  " + bezeichungliste[8] + " [24,90 EUR] (9)");
		
		do {
			System.out.print("Ihre Wahl:");
			auswahl = tastatur.nextInt();
		
			switch (auswahl) {
			case 1:
				ticketPreis = 2.9;
				break;
			case 2:
				ticketPreis = 3.3;
				break;
			case 3:
				ticketPreis = 3.6;
				break;
			case 4:
				ticketPreis = 1.9;
				break;
			case 5:
				ticketPreis = 8.6;
				break;
			case 6:
				ticketPreis = 9.0;
				break;
			case 7:
				ticketPreis = 23.5;
				break;
			case 8:
				ticketPreis = 24.3;
				break;
			case 9:
				ticketPreis = 24.9;
				break;
			default:
				System.out.println(">>falsche Eingabe<<");
			}
		} while (auswahl < 1 || auswahl > 9);

		System.out.print("Anzahl der Tickets: ");
		anzahlTickets = tastatur.nextInt();

		if (ticketPreis <= 0) {
			ticketPreis = 1;
		}

		if (anzahlTickets < 1 || anzahlTickets > 10) {
			anzahlTickets = 1;
		}

		return ticketPreis * anzahlTickets;
	}

	/**
	 * Erhält den zu zahlenden Betrag und fragt den User ab, bis er den Betrag
	 * bezahlt hat
	 * 
	 * @param zuZahlenderBetrag, Betrag der zuzahlen ist
	 * @return Rückgeld, wie viel überbezahlt wurde
	 */
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		double eingeworfenemuenze;
		double eingezahlterGesamtbetrag = 0.0;
		Scanner tastatur = new Scanner(System.in);

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.format("Noch zu zahlen: %4.2f Euro %n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfenemuenze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfenemuenze;
		}
		return eingezahlterGesamtbetrag - zuZahlenderBetrag;
	}

	/**
	 * Gibt animiert die Meldung aus, das bezahlt wurde
	 */
	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 28; i++) {
			System.out.print("=");
			try {
				Thread.sleep(150);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	/**
	 * ErhÃ¤lt dem RÃ¼ckgabebetrag und zahlt diesen in passenden MÃ¼nzen aus
	 */
	public static void rueckgeldAusgeben(double rueckgabebetrag) {

		if (rueckgabebetrag > 0.0) {
			System.out.format("Der Rückgabebetrag in Höhe von %4.2f Euro %n", rueckgabebetrag);
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rueckgabebetrag >= 2.0) {// 2 EURO-muenzen
				System.out.println("2 EURO");
				rueckgabebetrag -= 2.0;
			}
			while (rueckgabebetrag >= 1.0) {// 1 EURO-muenzen
				System.out.println("1 EURO");
				rueckgabebetrag -= 1.0;
			}
			while (rueckgabebetrag >= 0.5) // 50 CENT-muenzen
			{
				System.out.println("50 CENT");
				rueckgabebetrag -= 0.5;
			}
		}
	}
}